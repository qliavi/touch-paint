<?php

function get_run_revisions () {
    return [
        'compressed.css' => 41,
        'compressed.js' => 70,
    ];
}
