#!/usr/bin/env node

process.chdir(__dirname)
process.chdir('..')

var fs = require('fs')
var uglifyJs = require('uglify-js')

var files = [
    'AlphaSlider',
    'BarButton',
    'BucketTool',
    'ButtonExpandable',
    'Canvas',
    'ColorButton',
    'ColorButtonsPanel',
    'Div',
    'EditColorPanel',
    'EllipseTool',
    'FileButton',
    'FilePanel',
    'hsl2rgb',
    'HueSlider',
    'LineTool',
    'LuminanceSlider',
    'MainBar',
    'MainPanel',
    'OpenImage',
    'PalettePanel',
    'ParamsPanel',
    'PencilTool',
    'PickButton',
    'PickPanel',
    'PickTool',
    'RectangleTool',
    'rgb2hsl',
    'SaveCanvas',
    'SaturationSlider',
    'Slider',
    'ToolButton',
    'ToolPanel',
    'UndoButton',
    'Main',
]

var source = '(function () {\n'
files.forEach(function (file) {
    source += fs.readFileSync('js/' + file + '.js', 'utf8') + ';\n'
})
source += '\n})()'

var compressedSource = uglifyJs.minify({ 'combined.js': source }).code

fs.writeFileSync('combined.js', source)
fs.writeFileSync('compressed.js', compressedSource)
